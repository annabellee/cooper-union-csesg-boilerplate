import React from "react";
import Header from './Header';
import TopBar from './topbar';
import NavBar from './navbar';
import { Link } from "react-router-dom";

class ToS extends React.Component {
    render() {
        return (
        	<div>
        		<Header />
				<TopBar />
        		<h1> Terms of Service </h1>
        		<p> This site using the domain cfotsl.org is controlled by the Children's Foundation of Technology. 
        		CFOTSL provides to you, a website user, complimentary information service, at no charge, offered by the Children's Foundation of Technology- Sierra Leone with the express condition that users agree to be bound by the terms and conditions set forth below.
                </p>

                <p> 
                Please read the Terms of Use carefully before you start to use the website. 
                By using the website you acknowledge that you have read and agree to be bound and abide by these Terms of Service. If you do not agree with these Terms of Service, you must not access or use any of the website.
                Do not use the Website if you do not agree to these Terms of Service.
                </p>

                <p>
                We may revise and update these Terms of Service from time to time in our sole discretion without notice to you. 
                You must consult the most recent version of these Terms of Service each time you view the website. 
                Your continued use of the website following the posting of revised Terms of Service constitutes your acceptance of the Terms of Service as modified.
                </p>

                <p>
                All content available on this website, including, but not limited to, trademarks, photographs, images, characters, names, graphics, logos (including the design, selection and arrangement thereof), illustrations, audio clips and video clips (collectively, “content”), is protected by Copyright and other rights owned by the Children's Foundation of Technology. 
                You may use content from this website for your own personal, non-commercial use only. You may not copy, reproduce, publish, upload, post, transmit, store, create derivative works of, publicly display, distribute, or modify any content of the website in any way without CFoT’s express permission.
                </p>

                <p>
                If you use the website in breach of these Terms of Service, your right to use the website will cease immediately and you must, at our discretion, return or destroy any copies of any materials you have created. 
                No right, title or interest in or to the website or any content thereof is transferred to you, and all rights not expressly granted are reserved by the Children's Foundation of Technology. 
                Any use of the website not expressly permitted by these Terms of Service is a breach of these Terms of Service and may violate copyright, trademark and other applicable laws.
                </p>

                <p> 
                We reserve the right to withdraw or amend this website, and any service or material we provide on the website, in our sole discretion without notice. 
                We will not be liable if for any reason all or any part of the website is unavailable at any time or for any period.
                </p>

                <p>
                To access the website as a registered user, you may be asked to provide certain registration details or other information. 
                It is a condition of your use of the website that all the information you provide on the website is correct and complete. 
                You agree that all information you provide to us to register with this website or otherwise is governed by our <Link to = "/PP"> Privacy Policy</Link>, which is hereby incorporated by reference.
        		</p>

                <p> You may use the website only for lawful purposes and in accordance with these Terms of Service. You agree not to use the website: </p>
                <ul>
                    <li> In any way that violates any applicable federal, state, local, or international law or regulation (including, without limitation, any laws regarding the export of data or software to and from the US or other countries); </li>
                    <li> To send, knowingly receive, upload, download, use or re-use any material which does not comply with the Content Standards set out in these Terms of Service; </li>
                    <li> To transmit, or procure the sending of, any advertising or promotional, including any "junk mail", "chain letter" or "spam" or any other similar solicitation; </li>
                    <li> To impersonate or attempt to impersonate the Children's Foundation of Technology, a Children's Foundation of Technology employee, another user or any other person or entity (including, without limitation, by using e-mail addresses or screen names associated with any of the foregoing); </li>
                    <li> To engage in any other conduct that restricts or inhibits anyone's use or enjoyment of the website, or which, as determined by us, may harm the Children's Foundation of Technology or users of the website or expose them to liability. </li>    
                    <li> Use the website in any manner that could disable, overburden, damage, or impair the site or interfere with any other party's use of the website, including their ability to engage in real time activities through the website; </li>
                    <li> Use any robot, spider or other automatic device, process or means to access the website for any purpose, including monitoring or copying any of the material on the website; </li>
                    <li> Use any manual process to monitor or copy any of the material on the website or for any other unauthorized purpose without our prior written consent; </li>
                    <li> Use any device, software or routine that interferes with the proper working of the website; </li>
                    <li> Introduce any viruses, trojan horses, worms, logic bombs or other material which is malicious or technologically harmful; </li>
                    <li> Attempt to gain unauthorized access to, interfere with, damage or disrupt any parts of the website, the server on which the website is stored, or any server, computer or database connected to the website; </li>
                    <li> Attack the website via a denial-of-service attack or a distributed denial-of-service attack; </li>
                    <li> Otherwise attempt to interfere with the proper working of the website. </li>
                </ul>

                <p> No one under the age of 18 is allowed to register on the website. While the website is designed to be appropriate for all users, we will not knowingly collect personal information from users who are under the age of 18. If you are not yet 18 years old, please do not participate in any of the activities on the website that require you to provide personal information (like making a purchase or signing up to receive the newsletter).  </p>

                <h2> Disclaimer </h2>
                <p> The content on and available via this website is provided “as is” and without warranties of any kind either express or implied. To the fullest extent permissible pursuant to applicable law, the Children's Foundation of Technology disclaims all warranties, express or implied, including, without limitation, implied warranties of merchantability and fitness for a particular purpose. The Children's Foundation of Technology makes no warranty that the functions contained in the content will be uninterrupted or error-free, that defects will be corrected, or that the Children's Foundation of Technology or the server that makes them available are free of viruses or other harmful components. Applicable law may not allow the exclusion of implied warranties, so the above exclusion may not apply to you. </p>
                
                <h2> Limitation of Liability </h2>
                <p> In no event shall the Children's Foundation of Technology be liable for damages of any kind, under any legal theory, arising out or in connection with your use of, or inability to use, the website or any content on the website, including any direct, indirect, special, incidental, consequential, or punitive damages (including but not limited to, personal injury, pain and suffering, emotional, distress, loss of revenue, loss of profits, loss of business or anticipated savings, loss of use, loss of goodwill, loss of data, work stoppage, computer and/or device or technology failure or malfunction), and whether or not caused by (including negiglence), breach of contract or otherwise, even if foreseeable. 
                You agree that this limitation of liability applies whether such allegations are for breach of contract, tortious behavior, negligence, or fall under any other cause of action, regardless of the basis upon which liability is claimed and even if the Children's Foundation of Technology has been advised of the possibility of such loss or damage. Without limiting the generality of the foregoing, you also specifically acknowledge that the Children's Foundation of Technology and/or its present or future affiliates are not liable for any actual or alleged defamatory, offensive or illegal conduct of other users of the website or any other third parties.
                
                If applicable law does not allow all or any part of the above limitation of liability to apply to you, the limitations will apply to you only to the extent permitted by applicable law. </p>
                
                <h2> Your comments and concerns </h2>
                <p> All feedback, comments, requests for technical support and other communications relating to the website should be directed to cfotsl@gmail.com </p>

                <h2> Changes of these Terms of Services </h2>
                <p> The Children's Foundation of Technology reserves the right to change these Terms of Service any time, without notice to you. Please consut the most recent version of these Terms of Service each time you view the Website. 
                [Updated August, 2018] </p>

                <NavBar />
        	</div>
        )
    }
}

export default ToS; 
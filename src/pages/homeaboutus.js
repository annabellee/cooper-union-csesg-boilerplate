import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Menu from "@material-ui/core/Menu";
import Hidden from "@material-ui/core/Hidden";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Slideshow from "./slideshow";
import Header from "./Header";
import TopBar from "./topbar";
import NavBar from "./navbar";
import { createMuiTheme } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";
class homeaboutus extends React.Component {
	render() {
		return (
			<div>
			<Grid container spacing={16} justify="center">
				<Paper>
					<Grid item>
						<Grid>
							<h1> About Us </h1>
							<p>
								{" "}
								Our motto is "Future Heroes." The Children's
								Foundation of Technology is a nonprofit
								organization that strives to support the youth
								in Sierra Leone. We want to help the less
								privileged children and teach them technological
								and media skills so they can obtain a brighter
								future and eventually earn a living. The
								Children's Foundation of Technology wants to
								help the children grow up to be self-dependent
								adults. To know more, press Read More.{" "}
							</p>

							<Link
								to="/about"
								style={{
									color: "blue",
									textDecoration: "none"
								}}
							>
								<Tab label="Read More " />
							</Link>
						</Grid>
					</Grid>
				</Paper>
			</Grid>
			</div>
		);
	}
}
export default homeaboutus;

<Card style = {{maxWidth: 745, margin: ' auto', marginBottom: 10}}>
							<CardMedia style = {{width: '100%', margin: 'auto'}}>
								{this.props.post.featured_image ? 
									(
										<img
											src = {this.props.post.featured_image}
											style = {{width: '60%', margin: '0 150px 0 150px'}}
										/>
									)
									:
									(
										""
									)
								}
							
								{this.props.post.attachments ? 
									(
										<img
											src = {attachmentNum.URL}
											style = {{width: '60%', margin: '0 150px 0 150px'}}
										/>
									)
									:
									(
										""
									)
								}
							</CardMedia>

							<CardContent>
								<h1 style = {{textAlign: 'center'}} dangerouslySetInnerHTML={{ __html: this.props.post.title }}></h1>							

								

								<div style = {{textAlign: 'center'}} dangerouslySetInnerHTML={{ __html: this.props.post.excerpt }}></div>
							</CardContent>

							<CardActions style = {{justifyContent: 'center'}}>
								<HashLink to={ "/blog/" + this.props.post.ID} style = {{margin: '0 150px 0 300px;'}}>
									<button>Read More</button>
								</HashLink>
							</CardActions>
						</Card>
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { Link } from 'react-router-dom'


class SignupPage extends React.Component{
    render(){
	return(
	    <div>
		<h1>
		    Signup Page
		</h1>
		<Link to="/">Home</Link><br />"
		<Link to="/login">Login</Link>
	    </div>
	)
    }
};

export default SignupPage

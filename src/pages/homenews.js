import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Menu from "@material-ui/core/Menu";
import Hidden from "@material-ui/core/Hidden";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Slideshow from "./slideshow";
import Header from "./Header";
import TopBar from "./topbar";
import NavBar from "./navbar";
import { createMuiTheme } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";
class Homenews extends React.Component {
	render() {
		return (
			<Paper>
				<h1> News </h1>
				<p>
					Click here to learn about recent events occurring in Sierra
					Leone.
				</p>
				<Link
					to="/news"
					style={{ color: "blue", textDecoration: "none" }}
				>
					<Tab label="Read More" />
				</Link>
			</Paper>
		);
	}
}
export default Homenews;

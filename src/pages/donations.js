import React from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import { Link } from 'react-router-dom';
import construction from '../assets/imag/5.jpg';
import Hidden from '@material-ui/core/Hidden';
import NavBar from './navbar';
import Header from './Header';
import TopBar from './topbar';

class Donations extends React.Component{
	render()
	{
		return(

				<div>					

					<Header />
					<TopBar />

					<Hidden smDown>
					<div style = {{marginLeft: 105, marginRight: 105 }}>

						<Typography variant="display3" style = {{color: '#e55d25', fontWeight: 'bold', padding: '25px', width: '70%', margin: 'auto', fontFamily: 'Neue Haas Grotesk W01 Disp,Arial,sans-serif'}} component = "h1">
							<center>
								Donate Today
							</center>
						</Typography>

						
						<Grid container spacing = {12} style = {{marginBottom: 50}}>
							<Grid item sm = {4} style = {{backgroundColor: '#e55d25'}}>
								<Typography component = 'h1' variant = 'display1' style = {{color: 'white', lineHeight: 1.3, textAlign: 'left', marginTop: 25, marginLeft: 18, fontFamily: 'Neue Haas Grotesk W01 Disp,Arial,sans-serif', fontWeight: 'bold'}}>
									Together We Make a Difference
								</Typography>
								<Typography component = 'p' variant = 'title' style = {{color: 'white', textAlign: 'left', marginLeft: 18, marginRight: 18, marginTop: 20, lineHeight: 1.3, fontFamily: 'Neue Haas Grotesk W01 Disp,Arial,sans-serif', fontWeight: 'normal'}} >
									One donation at a time, you can make a difference. Provide the children with oppurtunities to explore their talents, even in Sierra Leone. Every donation is a step closer to a brighter future.
								</Typography>
								<Typography component = 'p' variant = 'title' style = {{color: 'white', textAlign: 'left', marginLeft: 18, marginRight: 18, marginTop: 20, lineHeight: 1.2, fontFamily: 'Neue Haas Grotesk W01 Disp,Arial,sans-serif', fontWeight: 'normal'}} >
									Thank You
								</Typography>
								<Typography variant = 'Body 2' style = {{color: 'white', textAlign: 'left', marginLeft: 18, marginRight: 18, marginTop: 5, marginBottom: 10, lineHeight: 1.2, fontFamily: 'Neue Haas Grotesk W01 Disp,Arial,sans-serif', fontWeight: 'lighter'}}>
									Mendy Kanu, founder of The Children's Foundation of Technology
								</Typography>
							</Grid>
							<Grid item sm = {8} style = {{backgroundImage: "url(" + construction + ")", paddingTop: '400px', backgroundRepeat: 'no-repeat'}}>
							</Grid>
						</Grid>	
						
						

						<Typography variant = "display1" component = "p" style = {{fontFamily: 'Raleway', fontSize: '1.5rem', marginBottom: 80}}>
							<center>
								Your donations will be used to help the CFoT school pay for education, reconstruction of school buildings, purchase school supplies, and training teachers. With your donations, the children in Sierra Leone will have better access to digital technology.
							</center>
						</Typography>

					<Grid container spacing = {12}>
						<Grid item sm = {2} />
						<Grid item sm = {8}>
							<Typography variant = "HeadLine" component = "p" style = {{fontFamily: 'Raleway', fontSize: '1.3rem', color: 'rgba(0, 0, 0, 0.54)', marginBottom: 100}}>
								<center>
									Supply the children with new laptops and other digital electronics for education! Drop off the supplies at
									<p style = {{fontsize: '0.1rem'}} >
									Jonathan Chin<br />
									1275 Sterling Place Apt 2A<br />
									Brooklyn, NY 11213
									</p>
									 The supplies will be delivered to Sierra Leone.
								</center>
							</Typography>
						</Grid>
						<Grid item sm = {2} />
					</Grid>

					<Typography variant = "display1" component = "p" style = {{fontFamily: 'Raleway', fontSize: '1.6rem', color: '#e55d25', marginBottom: 10}}>
						<center>
							Click the Button Down Below to Donate Now
						</center>	
					</Typography>

					<Grid container spacing = {12}>
						<Grid item sm = {4} />
						<Grid item sm = {4}>
							<Typography variant = "title" component = "p" style = {{fontFamily: 'Raleway', fontSize: '1rem', color: 'rgba(0, 0, 0, 0.54)', marginBottom: 30}}>
								<center>
									* Millersville University is a partner of The Children's Foundation of Technology in the United States.
								</center>
							</Typography>
						</Grid>
						<Grid item sm = {4} />
					</Grid>

					<center style = {{marginBottom: 40}}>
						<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" style = {{width: 210}}>
							<input type="hidden" name="cmd" value="_s-xclick" />
							<input type="hidden" name="hosted_button_id" value="F7UJSSLLK8X98" />
							<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style = {{backgroundColor: 'white'}}/>
							<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" />
						</form>
					</center>
					</div>
					</Hidden>


					<Hidden mdUp>

						<Typography variant="display3" style = {{color: '#e55d25', fontWeight: 'bold', padding: '25px', width: '70%', margin: 'auto', fontFamily: 'Neue Haas Grotesk W01 Disp,Arial,sans-serif'}} component = "h1">
							<center>
								Donate Today
							</center>
						</Typography>
						
						<Grid container spacing = {12} style = {{marginBottom: 50}}>
							<Grid item md = {12} style = {{backgroundColor: '#e55d25'}}>
								<Typography component = 'h1' variant = 'display1' style = {{color: 'white', lineHeight: 1.3, textAlign: 'left', marginTop: 25, marginLeft: 18, fontFamily: 'Neue Haas Grotesk W01 Disp,Arial,sans-serif', fontWeight: 'bold'}}>
									Together We Make a Difference
								</Typography>
								<Typography component = 'p' variant = 'title' style = {{color: 'white', textAlign: 'left', marginLeft: 18, marginRight: 18, marginTop: 20, lineHeight: 1.3, fontFamily: 'Neue Haas Grotesk W01 Disp,Arial,sans-serif', fontWeight: 'normal'}} >
									One donation at a time, you can make a difference. Provide the children with oppurtunities to explore their talents, even in Sierra Leone. Every donation is a step closer to a brighter future.
								</Typography>
								<Typography component = 'p' variant = 'title' style = {{color: 'white', textAlign: 'left', marginLeft: 18, marginRight: 18, marginTop: 20, lineHeight: 1.2, fontFamily: 'Neue Haas Grotesk W01 Disp,Arial,sans-serif', fontWeight: 'normal'}} >
									Thank You
								</Typography>
								<Typography variant = 'Body 2' style = {{color: 'white', textAlign: 'left', marginLeft: 18, marginRight: 18, marginTop: 5, marginBottom: 10, lineHeight: 1.2, fontFamily: 'Neue Haas Grotesk W01 Disp,Arial,sans-serif', fontWeight: 'lighter'}}>
									Mendy Kanu, founder of The Children's Foundation of Technology
								</Typography>
							</Grid>
						</Grid>	

						<Typography variant = "display1" component = "p" style = {{fontFamily: 'Raleway', fontSize: '1.5rem', marginBottom: 80}}>
							<center>
								Your donations will be used to help the CFoT school pay for education, reconstruction of school buildings, purchase school supplies, and training teachers. With your donations, the children in Serria Leone will have better access to digital technology.
							</center>
						</Typography>

					<Grid container spacing = {12}>
						<Grid item sm = {2} />
						<Grid item sm = {8}>
							<Typography variant = "HeadLine" component = "p" style = {{fontFamily: 'Raleway', fontSize: '1.3rem', color: 'rgba(0, 0, 0, 0.54)', marginBottom: 100}}>
								<center>
									Supply the children with new laptops and other digital electronics for education! Drop off the supplies at
									<p style = {{fontsize: '0.1rem'}} >
									Jonathan Chin<br />
									1275 Sterling Place Apt 2A<br />
									Brooklyn, NY 11213
									</p>
									The supplies will be delivered to Sierra Leone.
								</center>
							</Typography>
						</Grid>
						<Grid item sm = {2} />
					</Grid>

					<Typography variant = "display1" component = "p" style = {{fontFamily: 'Raleway', fontSize: '1.6rem', color: '#e55d25', marginBottom: 10}}>
						<center>
							Click the Button Down Below to Donate Now
						</center>	
					</Typography>

					<Grid container spacing = {12}>
						<Grid item sm = {4} />
						<Grid item sm = {4}>
							<Typography variant = "title" component = "p" style = {{fontFamily: 'Raleway', fontSize: '1rem', color: 'rgba(0, 0, 0, 0.54)', marginBottom: 30}}>
								<center>
									* Millersville University is a partner of The Children's Foundation of Technology in the United States.
								</center>
							</Typography>
						</Grid>
						<Grid item sm = {4} />
					</Grid>

					<center style = {{marginBottom: 40}}>
						<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" style = {{width: 210}}>
							<input type="hidden" name="cmd" value="_s-xclick" />
							<input type="hidden" name="hosted_button_id" value="F7UJSSLLK8X98" />
							<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style = {{backgroundColor: 'white'}}/>
							<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" />
						</form>
					</center>
					</Hidden>

			<NavBar />
		</div>
		)
	}
}

export default Donations;
import React from 'react';

import Grid from '@material-ui/core/Grid';

import mendyCamera from '../assets/imag/0.jpg';
import mendyComputer from '../assets/imag/1.jpg';
import graduation from '../assets/imag/2.jpg';
import kidsComputer from '../assets/imag/3.jpg';
import boyComputer from '../assets/imag/4.jpg';
import construction from '../assets/imag/5.jpg';
import mendyTeaching from '../assets/imag/6.jpg';
import groupPhoto from '../assets/imag/7.jpg';
import constructionPeople from '../assets/imag/8.jpg';
import girlComputer from '../assets/imag/9.jpg';
import bag from '../assets/imag/10.jpg';

import students from '../assets/imag/ss1.png';
import singer from '../assets/imag/ss2.png';
import shirt from '../assets/imag/ss3.png';
import futureheroes from '../assets/imag/ss4.png';
import Class from '../assets/imag/ss5.png'; 
import graduation2 from '../assets/imag/ss6.png';


import Header from './Header';
import TopBar from './topbar';
import Navbar from './navbar';

class Photos extends React.Component {
    render() {
	    return (
		    <div>
          <Header />
          <TopBar />

      <h1>
        <center>
          Photos
        </center>
      </h1>



          <div style={{ padding:20}}>
            <Grid style={{height:500}} container spacing={32} direction="row" align="center" >
              <Grid item md zeroMinWidth>
                <img src={mendyCamera} width="500" length="500" /> 
                  <p> Taking a Picture </p>
              </Grid>
              <Grid item md zeroMinWidth>
                 <img src={mendyComputer} width="500" length="500"/>
                  <p> Mr. Kanu using a Computer </p>
              </Grid>
            </Grid>
          </div> 

          <div style={{ padding:20}}>
            <Grid style={{height:500}} container spacing={32} direction="row" align="center" >
              <Grid item md zeroMinWidth>
                <img src={construction} width="500" length="500"/> 
                <p> A photo of the construction for the new school building</p> 
              </Grid>
              <Grid item md zeroMinWidth>
              <img src={constructionPeople} width="500" length="500"/>
              <p class="tab"/> <p> Construction of the new school building  </p>
            </Grid>
            </Grid>
          </div> 

          <div style={{ padding:20}}>
            <Grid style={{height:500}} container spacing={32} direction="row" align="center" >
              <Grid item md zeroMinWidth>
                <img src={girlComputer} width="500" length="500"/> 
                </Grid>
                <Grid item md zeroMinWidth>
                <img src={kidsComputer} width="500" length="500"/>                 
            </Grid>
            </Grid> 
            <p align = "center"> Students are Working with Computers </p>
          </div> 

          <div style={{ padding:20}}>
            <Grid style={{height:500}} container spacing={32} direction="row" align="center" >
              <Grid item md zeroMinWidth>
                <img src={boyComputer} width="500" length="500"/>    
              </Grid>
              <Grid item md zeroMinWidth>
              <img src={students} width="500" length="500" />  
            </Grid>
            </Grid>
             <p align = "center"> More students working with Computers </p>
          </div> 

          <div style={{ padding:20}}>
            <Grid style={{height:500}} container spacing={32} direction="row" align="center" >
              <Grid item md zeroMinWidth>
                <img src={graduation} width="500" length="500"/>    
                  </Grid>
                  <Grid item md zeroMinWidth>
                <img src={graduation2} width="500" length="500" />  
                  
              </Grid>
            </Grid>  <p align = "center"> Students Holding Certificates at Graduation</p>  
          </div> 


          <div style={{ padding:20}}>
            <Grid style={{height:500}} container spacing={32} direction="row" align="center" >
              <Grid item md zeroMinWidth>
                <img src={singer} width="500" length="500" />    
                  <p> Working in the studio!  </p>  
              </Grid>
              <Grid item md zeroMinWidth>
              <img src={shirt} width="500" length="500" />  
              <p> New shirts with the logo arrived </p> 
            </Grid>
            </Grid>
          </div> 


          <div style={{ padding:20}}>
            <Grid style={{height:500}} container spacing={32} direction="row" align="center" >
              <Grid item md zeroMinWidth>
                <img src={futureheroes} width="500" length="500" />   
                  <p> Students wear the shirts as they work </p>    
              </Grid>
              <Grid item md zeroMinWidth>
                <img src={mendyTeaching} width="500" length="500"/>    
                  <p> Mr. Kanu Teaching </p> 
              </Grid>
              
            </Grid>
          </div> 

          <div style={{ padding:20}}>
            <Grid style={{height:500}} container spacing={32} direction="row" align="center" >
              <Grid item md zeroMinWidth>
                <img src={groupPhoto} width="500" length="500"/>
                  <p> Picture of Mr. Kanu's students </p> 
              </Grid>
              <Grid item md zeroMinWidth>
                <img src={bag} width="500" length="500" />
                <p> Mr. Kanu provides free lunch for all the students. </p> 
                </Grid>
            </Grid>
          </div> 

          <div style={{ padding:20}}>
            <Grid style={{height:500}} container spacing={32} direction="column" align="center" >
            <Grid item md zeroMinWidth>
               <img src={Class} width="500" length="500" />   
               <p> The whole class together! </p> 
            </Grid>
          
          </Grid>
          </div>
          <Navbar />
		    </div>
	    );
    }
}


export default Photos 
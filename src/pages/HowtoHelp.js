import React from 'react';
import Header from './Header';
import Grid from '@material-ui/core/Grid';
import TopBar from './topbar';
import Navbar from './navbar';


class HowToHelp extends React.Component{
  render()
  {
    return(
    	<div>
    	<Header />
		<TopBar />
<Grid container spacing={24} >
        <Grid item xs={12} style={{marginRight:50, marginLeft:50}}> 
          
    	
			<h1 align = "center">
				How to Help
			</h1>

			<h2> Here are some simple, yet effective ways to contribute to the CFoTSL: </h2>
		<div class="row">
  			<div class="column">
   				 <h3>Become a Monthly Donor</h3>
    				<p>By donating money, you are helping the CFoT pay for education, fund the reconstruction of school buildings, provide school supplies, and train teachers to empower the youth.</p>
			  </div>
  		<div class="column">
    		<h3>Donate IPads, cords, or laptops</h3>
    			<p>Doing so will teach children how to use technology and foster their learning more easily. Any piece of technology will help the children learn how to use technology, because we only have five computers for thirty two students. 
			School supplies or books will help with their education, because there is a high illteracy rate in Sierra Leone. Another of our goals is to lower that rate.</p>
 			  </div>
		</div>

		<div class="row">
		  <div class="column">
		    <h3>Host fundraisers/events in your community</h3>
		    <p>A fundraising event would not only help us pay for materials, but also raise awareness about our cause.</p>
		  </div>
		  <div class="column">
		    <h3>Share the Word</h3>
		    <p>Raising awareness is a wonderful way to help us reach our goals.  The more people who help, the more students who are helped.</p>
		  </div>
		</div>
			
</Grid>
</Grid>

<Navbar />
</div>
)
}
}
export default HowToHelp;
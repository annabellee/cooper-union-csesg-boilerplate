import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import SkipNextIcon from '@material-ui/icons/SkipNext';

import lastdays from '../assets/lastdays.jpg'

import Header from './Header';
import TopBar from './topbar';
import Navbar from './navbar';

const styles = theme => ({
  card: {
    display: 'flex',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
    height: 151,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },
  playIcon: {
    height: 38,
    width: 38,
  },
});

class Videos extends React.Component{
  render()
  {
    return(
    	<div>
    	<Header />
		<TopBar />
    	
			<h1>
			<center style = {{marginBottom: 50}}>
				Videos
				</center>
			</h1>


 		<center> <iframe width="600" height="400" 
 			src="https://www.youtube.com/embed/T5BjO4iKNiE" />
 		</center> 
			<center style = {{marginBottom: 35}}> Video Editing Class with Mr. Kanu </center>
		<br/>
		
	    <center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/V9rMKuW4aX4"/>
		</center>
			<center style = {{marginBottom: 35}}> Pa Santigie Bangura Shares a Brief Video Story With Us </center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/G3fXorVjz_U"/>
		</center>
			<center style = {{marginBottom: 35}}> Water Problem in our Community: These are the Different Types of Waters and what we use them for. </center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/q95ai4pMuAk"/>
		</center>
			<center style = {{marginBottom: 35}}> Life about Ibrahim Sorie </center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/oO6bz9Zyre0"/>
		</center>
			<center style = {{marginBottom: 35}}> Last Days (Official Video) </center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/pQSkgDfN6fg"/>
		</center>
			<center style = {{marginBottom: 35}}> CFOT-SL Anthem (Official Video) </center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/sqYRnssC18g" />
		</center>
			<center style = {{marginBottom: 35}}> Children's Foundation of Technology School Computer Class </center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/w7_cM1-QGxQ" />
		</center>
			<center style = {{marginBottom: 35}}> Technology Building Process </center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/0Kt06DSOsvA"/>
		</center>
			<center style = {{marginBottom: 35}}> Presentation from CFOT-SL </center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/Ez_d7y26AqA"/>
		</center>
			<center style = {{marginBottom: 35}}> Kids Spent Many Hours Working Before Going to School or Even Eat...Check out this Video: </center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/MC5aElU9qLo"/>
		</center>
			<center style = {{marginBottom: 35}}>  Hunger Land Song Video...Preaching about Hunger in Africa Especially in Sierra Leone: Written by Mr. Mendy Kanu</center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/LJM7Edlk-Gw"/>
		</center>
			<center style = {{marginBottom: 35}}> Community Water Problem </center>
		<br/>

		<center> <iframe width="600" height="400"
			src="https://www.youtube.com/embed/uMXWSmILxn0"/>
		</center>
			<center style = {{marginBottom: 35}}> Future Generation Song Video - Real Life Story about Kids at Sierra Leone, Written by Mr. Mendy Kanu </center>
		<br/>

		<Navbar />
	</div>
)
}
}
export default Videos;
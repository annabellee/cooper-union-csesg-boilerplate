import React from 'react';
import Paper from '@material-ui/core/Paper';

class Stats extends React.Component{
  render()
  {
    return(
                <Paper id="card" style = {{marginBottom: 15}}>    
                        <ul> <h1> Why help Sierra Leone? </h1> </ul>
                        <p>               
                                    <ul>
                                    - Of those age 15 and over, only 48.1% can read and write English, Mende, Temne, or Arabic
                                    </ul>
                                    <ul>
                                    - The child labor rate (children ages 14 and under) is 48%
                                    </ul>
                                    <ul>
                                    - The percent of underweight children under the age of 5 is 18.1%, compared to America's 0.5%
                                    </ul>
                                    <ul>
                                    - There are 0.02 physicians and 0.4 hospital beds for every 1000 people
                                    </ul>
                                    <ul>
                                    -  More than 60% of the population is living on less than 1.25 USD a day
                                    </ul>
                                    <ul>
                                    - There was a civil war that lasted from 1991-2002 which destroyed schools and left thousands homeless 
                                    </ul>
                        </p>
                        </Paper>
                        )
    }
}

 export default Stats;